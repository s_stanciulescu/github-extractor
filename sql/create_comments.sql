create table if not exists comments(
	id INT  PRIMARY KEY auto_increment,
	github_id INT,
	html_url VARCHAR(255),
	repo_id INT,
	repo_name VARCHAR(100),
	user_login VARCHAR(100),
	user_github_id INT,
	commit_id VARCHAR(42),
	created_at VARCHAR(32),
	updated_at VARCHAR(32), 
	body LONGTEXT
);
