#Description
Application to retrieve data about a repository and its forks (arbitrarily depth of nested forks). 
The application retrieves meta-data from Github as jsons, which then are parsed to be inserted into a MySQL database.

#Setup
You need to have a MySQL server and set the credentials and URI to that server in database.txt

github.txt contains two tokens for querying the Github server. You can add two more tokens (generate them
from two different Github accounts, under personal token access). This will ensure that the tool will not
stop and sleep because it ran out of the 5000 requests. If it manages to consume the requests from all the
tokens (each token has 5000 requests, then it is banned for 1 hour), 
then it will sleep until the request limitation is reset by the Github server.  

#Usage via binary file
Binary file (jar) usage from target folder:
java -jar Github-Extractor-0.1-jar-with-dependencies.jar [github_user/repository_name] [option]

where [option] can be:
 --retrieve => Retrieves repository meta-data from Github 
 --populateDB => Creates a table with information from json files of the repository 
 --all => Combines retrieval, and populating database
 --main => same as all but only for the main repository, no forks are downloaded

E.g. java -jar Github-Extractor-0.1-jar-with-dependencies.jar mining-repos/bdd-helloworld --all

#Source files
Eclipse project. You need Scala 2.11.2 for compilation and Maven. 

#Not recently tested
This has not been tested recently. There may have been changes in Github's API. Use at your own risk. 

#License
All rights reserved (c) Stefan Stanciulescu 