package dk.itu.scas.github.extractor.util
/* Copyright (c) 2014-2015 Stefan Stanciulescu 
 * IT University of Copenhagen
 * 

This file is part of Github-Extractor.

    Github-Extractor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Github-Extractor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Github-Extractor.  If not, see <http://www.gnu.org/licenses/>.
    
 */
import scala.io.Source
import java.io.File
import scala.io.Codec
import java.nio.charset.CodingErrorAction
import java.io.FileNotFoundException
import java.io.IOException
object Util {

  implicit val codec = Codec("UTF-8")
  codec.onMalformedInput(CodingErrorAction.REPLACE)
  codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

  def readFromFile(path: String): String = {
    try {
      val file = Source.fromFile(new File(path))(codec).getLines.mkString
      return file
    } catch {
      case f: FileNotFoundException => {
        println("File not found...")
        return ""
      }
      case ex: IOException => {
        println("IOException...")
        return ""
      }
    }

  }
  def readFromFileLines(path: String): List[String] = {
    try {
      val file = Source.fromFile(new File(path))(codec).getLines.toList
      return file
    } catch {
      case f: FileNotFoundException => {
        println("File not found...")
        return List("")
      }
      case ex: IOException => {
        println("IOException...")
        return List("")
      }
    }
  }
}