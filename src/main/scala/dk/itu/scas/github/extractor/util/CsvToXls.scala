package dk.itu.scas.github.extractor.util
/* Copyright (c) 2014-2015 Stefan Stanciulescu 
 * IT University of Copenhagen
 * 

This file is part of Github-Extractor.

    Github-Extractor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Github-Extractor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Github-Extractor.  If not, see <http://www.gnu.org/licenses/>.
    
 */
import java.io.FileOutputStream
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import java.lang.Short
import au.com.bytecode.opencsv.CSVReader
import org.apache.poi.ss.usermodel._
import java.io.FileReader
import org.apache.poi.ss.util.NumberToTextConverter

object CsvToXls {

 def isNumeric(input: String): Boolean = input.forall(_.isDigit)
 def convertCSVToXLS(input: String, output: String) {
  val wb = new HSSFWorkbook();
  val helper = wb.getCreationHelper();
  val sheet = wb.createSheet("new sheet");
  
  val reader = new CSVReader(new FileReader(input));
  var line = Array[String]()
  var r = 0;
  //val iterator = Iterator.continually(reader.readNext()).takeWhile(_ != null)
  while ({ line = reader.readNext; line != null }) {
   r = r + 1
   val row = sheet.createRow(r);

   for (i <- 0 until line.length) {

    try {
     row.createCell(i).setCellValue(line(i).toInt)
    } catch {

     case e: NumberFormatException => row.createCell(i).setCellValue(helper.createRichTextString(line(i)));

    }
   }
  }

  // Write the output to a file
  val fileOut = new FileOutputStream(output);
  wb.write(fileOut);
  fileOut.close();
 }
}