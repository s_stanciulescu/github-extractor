package dk.itu.scas.github.extractor
/* Copyright (c) 2014-2015 Stefan Stanciulescu 
 * IT University of Copenhagen
 * 

This file is part of Github-Extractor.

    Github-Extractor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Github-Extractor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Github-Extractor.  If not, see <http://www.gnu.org/licenses/>.
    
 */
import java.io.File
import org.joda.time.DateTime
import java.io.FilenameFilter
import java.sql._

import scala.io.Source

import com.mysql.jdbc.PreparedStatement

import argonaut._
import argonaut.Argonaut._
import dk.itu.scas.github.extractor._
import dk.itu.scas.github.extractor.util.Util._

object PopulateDatabase {

  def getDatabaseConnection: java.sql.Connection = {
    val databaseSettings = dk.itu.scas.github.extractor.util.Util.readFromFileLines("database.txt");
    val url = databaseSettings(0).split(":")(1)
    val port = databaseSettings(1).split(":")(1)
    val user = databaseSettings(2).split(":")(1)
    val pass = databaseSettings(3).split(":")(1)
    val dbc = "jdbc:mysql://" + url + ":" + port + "/"
    classOf[com.mysql.jdbc.Driver]
    val connection = DriverManager.getConnection(dbc, user, pass)
    return connection
  }

  def buildTables(repoName: String) {

    val connection = getDatabaseConnection
    val dropSchema = "drop schema if exists github_extractor_" + repoName.replace("-", "_")
    //println(dropSchema)
    val dropSchemaStmt = connection.prepareStatement(dropSchema)
    dropSchemaStmt.execute
    val createSchema = "create schema if not exists github_extractor_" + repoName.replace("-", "_")
    val createSchemaStmt = connection.prepareStatement(createSchema)
    createSchemaStmt.execute
    connection.setCatalog("github_extractor_" + repoName.replace("-", "_"))
    println("Creating tables...")
    val createRepository = Source.fromFile(new File("sql/create_repository.sql")).getLines.mkString("\n")
    val createUsers = Source.fromFile(new File("sql/create_users.sql")).getLines.mkString("\n")
    val createCommits = Source.fromFile(new File("sql/create_commits.sql")).getLines.mkString("\n")
    val createBranches = Source.fromFile("sql/create_branches.sql").getLines.mkString("\n")
    val createBranchCommits = Source.fromFile("sql/create_branch_commits.sql").getLines.mkString("\n")
    val alterBranchCommits = Source.fromFile("sql/alter_branch_commits.sql").getLines.mkString("\n")
    val createIssues = Source.fromFile("sql/create_issues.sql").getLines.mkString("\n")
    val createComments = Source.fromFile("sql/create_comments.sql").getLines.mkString("\n")
    val createPullRequests = Source.fromFile("sql/create_pull_requests.sql").getLines.mkString("\n")
    val createRepositoryStmt = connection.prepareStatement(createRepository)
    val createUsersStmt = connection.prepareStatement(createUsers)
    val createCommitsStmt = connection.prepareStatement(createCommits)
    val createBranchesStmt = connection.prepareStatement(createBranches)
    val createBranchCommitsStmt = connection.prepareStatement(createBranchCommits)
    val alterBranchCommitsStmt = connection.prepareStatement(alterBranchCommits)
    val createIssuesStmt = connection.prepareStatement(createIssues)
    val createCommentsStmt = connection.prepareStatement(createComments)
    val createPullRequestsStmt = connection.prepareStatement(createPullRequests)
    createRepositoryStmt.execute
    createUsersStmt.execute
    createCommitsStmt.execute
    createBranchesStmt.execute
    createBranchCommitsStmt.execute
    alterBranchCommitsStmt.execute
    createIssuesStmt.execute
    createCommentsStmt.execute
    createPullRequestsStmt.execute
    println("Done")
    connection.close
  }

  def insertAllRepositories(directory: String) = {
    val connection = getDatabaseConnection
    connection.setCatalog("github_extractor_" + directory.split("/")(1).replace("-", "_"))
    val (repository, commitsMaps, branches) = getMainRepositoryData(directory)
    val (userId, mainUserLogin, mainUserName, mainUserEmail, mainUserCreatedAt) = getRepository(directory)

    // ****** REPOSITORY ********* //
    val repoId = insertRepository(connection, repository.id, repository.full_name, repository.created_at, repository.updated_at, repository.pushed_at, repository.owner.login, mainUserName, mainUserEmail, repository.forks_count, "", 0, false)
    repoId.next
    // ****** USER *************** //
    insertUser(connection, userId, mainUserLogin, mainUserName, mainUserEmail, mainUserCreatedAt)

    // ****** ISSUES **********//

    val issues = readFromFile(directory + "/issues.json").decodeOption[List[Issues]].get
    insertIssues(connection, repoId.getInt(1), repository.full_name, issues)

    // ****** COMMENTS **********//
    val comments = readFromFile(directory + "/comments.json").decodeOption[List[Comments]].get
    insertComments(connection, repoId.getInt(1), repository.full_name, comments)

    // ***** PULL_REQUESTS *******//

    val pullRequestFiles = new File(directory + "/pulls/").listFiles(new FilenameFilter {
      override def accept(f: File, filename: String): Boolean = {
        if (filename.endsWith(".json"))
          return true
        else return false
      }
    })
    val pullRequests = pullRequestFiles.map(x => readFromFile(x.getPath).decodeOption[SinglePullRequest].get).toList
    insertPullRequests(connection, repoId.getInt(1), repository.full_name, pullRequests)

    // ****** BRANCHES && COMMITS*********** //

    val psCommit = connection.prepareStatement("insert into commits (sha, repo_id, repo_name, author_name, author_date, committer_name, committer_date, message) VALUES (?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE sha=sha;", Statement.RETURN_GENERATED_KEYS)
    val psGetCommitId = connection.prepareStatement("SELECT id, sha FROM commits WHERE sha = ? ")
    branches.foreach(branch => {
      // BRANCHES
      val branchCommits = commitsMaps.get(branch.name).get
      val id = insertBranches(connection, branch.name, repoId.getInt(1), branchCommits.size, branch.commit.sha)
      id.next
      val idCommitBranchMap = collection.mutable.Map[Int, Int]()
      branchCommits.foreach(x => {
        psGetCommitId.setString(1, x.sha)
        val result = psGetCommitId.executeQuery
        var commitId = 0
        if (result.next)
          commitId = result.getInt(1)
        else {
          val result = insertCommit(connection, x.sha, repoId.getInt(1), repository.full_name, x.commit.get.author.get.name, x.commit.get.author.get.date, x.commit.get.committer.get.name, x.commit.get.committer.get.date, x.commit.get.message, psCommit)
          result.next
          commitId = result.getInt(1)
        }
        idCommitBranchMap += (commitId -> id.getInt(1))
        //insertBranchCommit(connection, commitId, id.getInt(1))    // the commit exists
      })

      insertIdBranchesCommits(connection, idCommitBranchMap.toMap)
    })

    // *** FORKS **** //
    val forkLevelDirectory = new File(directory + "/forks").listFiles()
    if (!forkLevelDirectory.isEmpty) {
      forkLevelDirectory.foreach(forks => {
        val fork_level = forks.getName.split("-")(1)
        val forkDirectory = forks.listFiles
        // FOR EACH FORK INSERT REPOSITORY, USER, BRANCHES, COMMITS, BRANCH-COMMITS
        for (fork <- forkDirectory) {
          val (forkUserId, forkUserLogin, forkUserName, forkUserEmail, forkUserCreatedAt) = getRepository(fork.getPath)
          val (forkrepository, forkCommits, forkBranches) = getRepositoryData(fork.getPath)

          // INSERT REPOSITORY //    
          val forkRepoId = insertRepository(connection, forkrepository.id, forkrepository.full_name, forkrepository.created_at, forkrepository.updated_at, forkrepository.pushed_at, forkUserLogin, forkUserName, forkUserEmail, forkrepository.forks_count, forkrepository.parent.full_name, fork_level.toInt, false)
          forkRepoId.next
          // INSERT USER //
          insertUser(connection, forkUserId, forkUserLogin, forkUserName, forkUserEmail, forkUserCreatedAt)

          // ****** ISSUES **********//

          val forkIssues = readFromFile(fork.getPath + "/issues.json").decodeOption[List[Issues]].get
          insertIssues(connection, forkRepoId.getInt(1), forkrepository.full_name, forkIssues)

          // ****** COMMENTS **********//
          val forkComments = readFromFile(fork.getPath + "/comments.json").decodeOption[List[Comments]].get
          insertComments(connection, repoId.getInt(1), forkrepository.full_name, forkComments)

          // ***** PULL_REQUESTS *******//

          val forkPullRequestFiles = new File(fork.getPath + "/pulls/").listFiles(new FilenameFilter {
            override def accept(f: File, filename: String): Boolean = {
              if (filename.endsWith(".json"))
                return true
              else return false
            }
          })
          val forkPullRequests = forkPullRequestFiles.map(x => readFromFile(x.getPath).decodeOption[SinglePullRequest].get).toList
          insertPullRequests(connection, forkRepoId.getInt(1), forkrepository.full_name, forkPullRequests)

          val psForkCommit = connection.prepareStatement("insert into commits (sha, repo_id, repo_name, author_name, author_date, committer_name, committer_date, message) VALUES (?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE sha=sha;", Statement.RETURN_GENERATED_KEYS)
          val psForkGetCommitId = connection.prepareStatement("SELECT id, sha FROM commits WHERE sha = ? ")
          // INSERT BRANCHES, COMMITS, BRANCH-COMMITS //
          forkBranches.foreach(branch => {

            // BRANCHES
            val forkBranchCommits = forkCommits.get(branch.name).get
            val forkBranchId = insertBranches(connection, branch.name, forkRepoId.getInt(1), forkBranchCommits.size, branch.commit.sha)
            forkBranchId.next

            val forkIdCommitBranchMap = collection.mutable.Map[Int, Int]()
            forkBranchCommits.foreach(x => {
              psForkGetCommitId.setString(1, x.sha)
              val result = psForkGetCommitId.executeQuery
              var forkCommitId = 0
              if (result.next)
                forkCommitId = result.getInt(1)
              else {
                val result = insertCommit(connection, x.sha, forkRepoId.getInt(1), forkrepository.full_name, x.commit.get.author.get.name, x.commit.get.author.get.date, x.commit.get.committer.get.name, x.commit.get.committer.get.date, x.commit.get.message, psForkCommit)
                result.next
                forkCommitId = result.getInt(1)
              }
              forkIdCommitBranchMap += (forkCommitId -> forkBranchId.getInt(1))
            })
            insertIdBranchesCommits(connection, forkIdCommitBranchMap.toMap)
          })

        }

      })
    }

  }

  def getMainRepositoryData(directory: String): (MainRepository, Map[String, List[Commits]], List[Branch]) = {
    val mainRepository = readFromFile(directory + "/repo.json").decodeOption[MainRepository].get
    val branches = readFromFile(directory + "/branches.json").decodeOption[List[Branch]].getOrElse(Nil)
    val commitsMap = collection.mutable.Map[String, List[Commits]]()
    branches.foreach(x => {
      val name = x.name.replace("/", "-")
      val commits = readFromFile(directory + "/branch/" + name + "-commits.json").decodeOption[List[Commits]].get
      commitsMap += (x.name -> commits)

    })
    return (mainRepository, commitsMap.toMap, branches)
  }

  def getRepositoryData(directory: String): (Repository, Map[String, List[Commits]], List[Branch]) = {
    val repository = readFromFile(directory + "/repo.json").decodeOption[Repository].get
    val branches = readFromFile(directory + "/branches.json").decodeOption[List[Branch]].getOrElse(Nil)
    val commitsMap = collection.mutable.Map[String, List[Commits]]()
    branches.foreach(x => {
      val name = x.name.replace("/", "-")
      val commits = readFromFile(directory + "/branch/" + name + "-commits.json").decodeOption[List[Commits]].get
      commitsMap += (x.name -> commits)

    })
    return (repository, commitsMap.toMap, branches)
  }
  def getRepository(directory: String): (Int, String, String, String, String) = {
    val repoJson = readFromFile(directory + "/repo.json")
    val repoJsonDecoded = repoJson.decodeOption[MainRepository].get
    val repoOwner = repoJsonDecoded.owner.login
    val userJson = readFromFile(directory + "/user.json")
    //println(userJson)
    var userId = 0
    var userLogin = ""
    var userName = ""
    var userEmail = ""
    var userCreatedAt = ""

    val userJsonDecoded = userJson.decodeOption[User].getOrElse(Nil)
    if (userJsonDecoded != Nil) {
      val userJsonDecoded1 = userJson.decodeOption[User].get
      userName = userJsonDecoded1.name
      userLogin = userJsonDecoded1.login
      userEmail = userJsonDecoded1.email
      userCreatedAt = userJsonDecoded1.created_at
      userId = userJsonDecoded1.id

    } else {
      val userJsonDecoded1 = userJson.decodeOption[UserNoEmail].getOrElse(Nil)
      if (userJsonDecoded1 != Nil) {
        userId = userJson.decodeOption[UserNoEmail].get.id
        userLogin = userJson.decodeOption[UserNoEmail].get.login
        userCreatedAt = userJson.decodeOption[UserNoEmail].get.created_at
        userName = userJson.decodeOption[UserNoEmail].get.name

      } else {
        val userJsonDecoded2 = userJson.decodeOption[UserNoName].getOrElse(Nil)
        userId = userJson.decodeOption[UserNoName].get.id
        userLogin = userJson.decodeOption[UserNoName].get.login
        userCreatedAt = userJson.decodeOption[UserNoName].get.created_at
      }
    }

    return (userId, repoOwner, userName, userEmail, userCreatedAt)
  }

  def insertRepository(connection: java.sql.Connection, github_id: Int, repo_name: String, created_at: String, updated_at: String, pushed_at: String, repo_owner_login: String, repo_owner_name: String, repo_owner_email: String, forks_count: Int, forked_from: String, fork_level: Int, active_fork: Boolean): ResultSet = {
    val preparedStatement = connection.prepareStatement("""insert into repository(github_id, repo_name, created_at, updated_at, pushed_at, repo_owner_login,repo_owner_name, repo_owner_email, forks_count, forked_from, fork_level, active_fork)
		  values (?,?,?,?,?,?,?,?,?,?,?,?)""", Statement.RETURN_GENERATED_KEYS)

    val activefork = new DateTime(pushed_at.mkString).isAfter(new DateTime(created_at.mkString))
    preparedStatement.setInt(1, github_id)
    preparedStatement.setString(2, repo_name)
    preparedStatement.setString(3, created_at)
    preparedStatement.setString(4, updated_at)
    preparedStatement.setString(5, pushed_at)
    preparedStatement.setString(6, repo_owner_login)
    preparedStatement.setString(7, repo_owner_name)
    preparedStatement.setString(8, repo_owner_email)
    preparedStatement.setInt(9, forks_count)
    preparedStatement.setString(10, forked_from)
    preparedStatement.setInt(11, fork_level)
    preparedStatement.setBoolean(12, activefork)
    preparedStatement.executeUpdate()
    return preparedStatement.getGeneratedKeys()
  }

  def insertUser(connection: java.sql.Connection, user_github_id: Int, user_login: String, user_name: String, user_email: String, created_at: String) {
    val preparedStatement = connection.prepareStatement("""insert into users (user_github_id, user_login, user_name, user_email, created_at) values 
   (?,?,?,?,?)""")
    preparedStatement.setInt(1, user_github_id)
    preparedStatement.setString(2, user_login)
    preparedStatement.setString(3, user_name)
    preparedStatement.setString(4, user_email)
    preparedStatement.setString(5, created_at)
    preparedStatement.execute
  }

  def insertBranches(connection: java.sql.Connection, branchName: String, repoId: Int, nrCommits: Int, lastCommitSHA: String): ResultSet = {
    val preparedStatement = connection.prepareStatement(""" insert into branches (branch_name, repo_id, nr_commits, last_commit_sha) VALUES (?,?,?,?) """, Statement.RETURN_GENERATED_KEYS)
    preparedStatement.setString(1, branchName)
    preparedStatement.setInt(2, repoId)
    preparedStatement.setInt(3, nrCommits)
    preparedStatement.setString(4, lastCommitSHA)
    preparedStatement.executeUpdate
    return preparedStatement.getGeneratedKeys
  }

  def insertCommitBatch(connection: java.sql.Connection): java.sql.PreparedStatement = {
    val preparedStatement = connection.prepareStatement("insert into commits (sha, repo_id, repo_name, author_name, author_date, committer_name, committer_date, message) VALUES (?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE sha=sha;", Statement.RETURN_GENERATED_KEYS)
    return preparedStatement

  }

  def insertPullRequests(connection: java.sql.Connection, repoId: Int, repoName: String, pullRequests: List[SinglePullRequest]) = {
    val preparedStatement = connection.prepareStatement("""
     insert into pull_requests(github_id,repo_id,repo_name, from_repo_name, pullrequest_number,title,body,user_login, user_github_id,state,created_at, updated_at,
                               closed_at, merged_at,merge_commit_sha,merged,merged_by_login,nr_of_comments,nr_of_commits,additions,deletions,changed_files) VALUES(
                               ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)""")
    connection.setAutoCommit(false)
    pullRequests.foreach(pullRequest => {

      preparedStatement.setLong(1, pullRequest.id)
      preparedStatement.setLong(2, repoId)
      preparedStatement.setString(3, repoName)

      try {
        pullRequest.head.get.repo match {
          case Some(SimpleRepository(_, _)) => preparedStatement.setString(4, pullRequest.head.get.repo.get.full_name)
          case None                         => preparedStatement.setString(4, "unknown")
        }
      } catch {
        case ex: java.util.NoSuchElementException => preparedStatement.setString(4, "unknown")
      }
      preparedStatement.setLong(5, pullRequest.number)
      preparedStatement.setString(6, pullRequest.title)
      try {
        preparedStatement.setString(7, pullRequest.body.get)
      } catch {
        case ex: java.util.NoSuchElementException => preparedStatement.setString(7, "")
      }
      val user = pullRequest.user match {
        case Some(SimpleUser(_, _)) => { preparedStatement.setString(8, pullRequest.user.get.login); preparedStatement.setLong(9, pullRequest.user.get.id); }
        case None => {
          preparedStatement.setString(8, "Null")
          preparedStatement.setLong(9, 0)
        }
      }
      preparedStatement.setString(10, pullRequest.state)
      preparedStatement.setString(11, pullRequest.created_at)
      preparedStatement.setString(12, pullRequest.updated_at.getOrElse("Null"))
      preparedStatement.setString(13, pullRequest.closed_at.getOrElse("Null"))
      preparedStatement.setString(14, pullRequest.merged_at.getOrElse("Null"))
      preparedStatement.setString(15, pullRequest.merge_commit_sha.getOrElse("Null"))
      preparedStatement.setBoolean(16, pullRequest.merged)
      val mergedBy = pullRequest.merged_by match {
        case Some(SimpleUser(_, _)) => preparedStatement.setString(17, pullRequest.merged_by.get.login)
        case None                   => preparedStatement.setString(17, "Null")
      }
      preparedStatement.setLong(18, pullRequest.comments.getOrElse(0))
      preparedStatement.setLong(19, pullRequest.commits.getOrElse(0))
      preparedStatement.setLong(20, pullRequest.additions.getOrElse(0))
      preparedStatement.setLong(21, pullRequest.deletions.getOrElse(0))
      preparedStatement.setLong(22, pullRequest.changed_files.getOrElse(0))

      preparedStatement.addBatch

    })
    preparedStatement.executeBatch
    connection.commit
  }
  def insertComments(connection: java.sql.Connection, repoId: Int, repoName: String, comments: List[Comments]) = {
    val preparedStatement = connection.prepareStatement("insert into comments (github_id, html_url,repo_id, repo_name, user_login, user_github_id, commit_id, created_at, updated_at, body) VALUES (?,?,?,?,?,?,?,?,?,?)")
    connection.setAutoCommit(false)
    comments.foreach(comment => {
      preparedStatement.setLong(1, comment.id)
      preparedStatement.setString(2, comment.html_url)
      preparedStatement.setLong(3, repoId)
      preparedStatement.setString(4, repoName)
      preparedStatement.setString(5, comment.user.login)
      preparedStatement.setLong(6, comment.user.id)
      preparedStatement.setString(7, comment.commit_id.getOrElse("Null"))
      preparedStatement.setString(8, comment.created_at)
      preparedStatement.setString(9, comment.updated_at.getOrElse("Null"))
      preparedStatement.setString(10, comment.body.getOrElse("Null"))
      preparedStatement.addBatch
    })

    preparedStatement.executeBatch
    connection.commit
  }
  def insertIssues(connection: java.sql.Connection, repoId: Int, repoName: String, issues: List[Issues]) = {
    val preparedStatement = connection.prepareStatement("insert into issues (github_id,repo_id, repo_name, issue_number, title, user_login, user_github_id, state, nr_of_comments, created_at, updated_at, closed_at, body) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)")
    connection.setAutoCommit(false)
    issues.foreach(issue => {
      preparedStatement.setLong(1, issue.id)
      preparedStatement.setLong(2, repoId)
      preparedStatement.setString(3, repoName)
      preparedStatement.setLong(4, issue.number)
      preparedStatement.setString(5, issue.title)
      preparedStatement.setString(6, issue.user.login)
      preparedStatement.setLong(7, issue.user.id)
      preparedStatement.setString(8, issue.state)
      preparedStatement.setLong(9, issue.comments)
      preparedStatement.setString(10, issue.created_at)
      preparedStatement.setString(11, issue.updated_at.getOrElse("Null"))
      preparedStatement.setString(12, issue.closed_at.getOrElse("Null"))
      preparedStatement.setString(13, issue.body.getOrElse("Null"))
      preparedStatement.addBatch()

    })
    preparedStatement.executeBatch
    connection.commit
  }

  def insertCommits(connection: java.sql.Connection, repoId: Int, repoName: String, commits: List[Commits]): ResultSet = {
    val preparedStatement = insertCommitBatch(connection)
    connection.setAutoCommit(false)

    commits.foreach(commit => {
      val author_name1 = (commit.commit.get.author.get.name)
      val author_date1 = commit.commit.get.author.get.date
      val committer_name1 = (commit.commit.get.committer.get.name)
      val committer_date1 = commit.commit.get.committer.get.date
      val commitMessage1 = ((commit.commit.get.message))

      preparedStatement.setString(1, commit.sha)
      preparedStatement.setInt(2, repoId)
      preparedStatement.setString(3, repoName)

      preparedStatement.setString(4, author_name1)
      preparedStatement.setString(5, author_date1)
      preparedStatement.setString(6, committer_name1)
      preparedStatement.setString(7, committer_date1)
      preparedStatement.setString(8, commitMessage1)
      preparedStatement.addBatch()

    })
    preparedStatement.executeBatch
    connection.commit
    return preparedStatement.getGeneratedKeys
  }
  def insertCommit(connection: java.sql.Connection, sha: String, repoId: Int, repoName: String, authorName: String, authorDate: String, committerName: String, committerDate: String, message: String, preparedStatement: java.sql.PreparedStatement): ResultSet = {
    //val preparedStatement = connection.prepareStatement("insert into commits (sha, repo_id, repo_name, author_name, author_date, committer_name, committer_date, message) VALUES (?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE sha=sha;", Statement.RETURN_GENERATED_KEYS)
    preparedStatement.setString(1, sha)
    preparedStatement.setInt(2, repoId)
    preparedStatement.setString(3, repoName)

    preparedStatement.setString(4, authorName)
    preparedStatement.setString(5, authorDate)
    preparedStatement.setString(6, committerName)
    preparedStatement.setString(7, committerDate)
    preparedStatement.setString(8, message)
    preparedStatement.executeUpdate
    return preparedStatement.getGeneratedKeys()
  }

  def insertBranchCommitsBatch(connection: java.sql.Connection): java.sql.PreparedStatement = {
    val preparedStatement = connection.prepareStatement("""insert into branches_commits (commit_id, branch_id) VALUES (?,?)""")
    return preparedStatement
  }

  def insertBranchCommit(connection: java.sql.Connection, commitId: Int, branchId: Int) {
    val preparedStatement = connection.prepareStatement("""insert into branches_commits (commit_id, branch_id) VALUES (?,?)""")
    preparedStatement.setInt(1, commitId)
    preparedStatement.setInt(2, branchId)
    preparedStatement.executeUpdate
  }
  def insertBranchCommits(connection: java.sql.Connection, ids: ResultSet, branchId: Int) {
    val preparedStatement = insertBranchCommitsBatch(connection)
    connection.setAutoCommit(false)
    while (ids.next) {
      preparedStatement.setInt(1, ids.getInt(1))
      preparedStatement.setInt(2, branchId)
      preparedStatement.addBatch
    }
    preparedStatement.executeBatch
    connection.commit
  }

  def insertIdBranchesCommits(connection: java.sql.Connection, ids: Map[Int, Int]) {
    val preparedStatement = insertBranchCommitsBatch(connection)
    connection.setAutoCommit(false)

    ids.foreach(x => {
      preparedStatement.setInt(1, x._1)
      preparedStatement.setInt(2, x._2)
      preparedStatement.addBatch

    })
    preparedStatement.executeBatch
    connection.commit
  }

  def populateDatabase(repoName: String) {
    buildTables(repoName.replace("/", "-"))
    insertAllRepositories("github/" + repoName.replace("/", "-"))
  }
}