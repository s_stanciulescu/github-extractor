package dk.itu.scas.github.extractor.util
/* Copyright (c) 2014-2015 Stefan Stanciulescu 
 * IT University of Copenhagen
 * 

This file is part of Github-Extractor.

    Github-Extractor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Github-Extractor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Github-Extractor.  If not, see <http://www.gnu.org/licenses/>.
    
 */
import java.io.File
import java.nio.charset.CodingErrorAction

import scala.io.Codec

import org.apache.commons.io.FileUtils

import argonaut.Parse

object JsonParse {
 implicit val codec = Codec("UTF-8")
 codec.onMalformedInput(CodingErrorAction.REPLACE)
 codec.onUnmappableCharacter(CodingErrorAction.REPLACE)
 
 def checkParseErrors(directory: String) {
  val extensions = Array("json")
  val allJsons = FileUtils.listFiles(new File(directory), extensions, true)
  val it = allJsons.iterator()
  while (it.hasNext()) {
   val file = it.next
   val parse = Parse.parse(Util.readFromFile(file.getPath))
   if (parse.validation.isFailure)
    println(file.getPath)

  }
 }
}