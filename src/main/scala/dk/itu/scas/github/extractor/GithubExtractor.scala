package dk.itu.scas.github.extractor
/* Copyright (c) 2014-2015 Stefan Stanciulescu 
 * IT University of Copenhagen
 * 

This file is part of Github-Extractor.

    Github-Extractor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Github-Extractor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Github-Extractor.  If not, see <http://www.gnu.org/licenses/>.
    
 */

import java.io._

import scala.collection.mutable.StringBuilder
import scala._
import scalaz._
import scala.io.Source
import java.net.URL
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import java.text.DateFormat
import argonaut._
import Argonaut._
import scala.sys.process.Process
import sys.process._
import scala.sys.process.ProcessLogger
import scala.util.control.Breaks._
import scala.collection.immutable.HashMap
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import scala.collection.mutable.MutableList
import scalaj.http.Http
import scalaj.http._
import com.google.gson._
import scala.io.Codec
import java.nio.charset.CodingErrorAction
import java.io.FileNotFoundException

import dk.itu.scas.github.extractor.util.Util._
import dk.itu.scas.github.extractor.PopulateDatabase._

object GithubExtractor {

  // **** CODEC RELATED FIXES ****//
  implicit val codec = Codec("UTF-8")
  codec.onMalformedInput(CodingErrorAction.REPLACE)
  codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

  val forks = "forks"
  val commits = "commits"
  val branches = "branches"
  val threads = 4
  val ETagHeader = "If-None-Match"
  val accessToken = "access_token"
  val api = "https://api.github.com/"
  val api_repo = "https://api.github.com/repos/"
  val api_users = "https://api.github.com/users/"
  val rate_limit = "rate_limit"
  val TIMEOUT = 20000
  val auth = Source.fromFile(new File("github.txt")).getLines.toList
  val tokenList = auth

  var token1 = auth(0).split(";")(0)
  var token2 = auth(1).split(";")(0)
  var token3 = auth(2).split(";")(0)
  var token4 = auth(3).split(";")(0)

  var authentication = token1
  var currentToken = token1
  var totalNumberOfForks = 0
  var requests = 0
  val gson = new GsonBuilder().setPrettyPrinting().create
  val jsonParser = new JsonParser
  tokens()
  def getCurrentDirectory = System.getProperty("user.dir")

  def request(url: String): (Int, Map[String, String], String) = {
    val requestCall: HttpResponse[String] = Http(url).options(HttpOptions.connTimeout(TIMEOUT), HttpOptions.readTimeout(TIMEOUT)).asString
    
    requestCall.code match {
      case 200 => return (requestCall.code, requestCall.headers, requestCall.body)
      case 301 => {
        val secondRequestCall: HttpResponse[String] = Http(url).options(HttpOptions.connTimeout(TIMEOUT), HttpOptions.readTimeout(TIMEOUT), HttpOptions.followRedirects(true)).asString
        return (secondRequestCall.code, secondRequestCall.headers, secondRequestCall.body)
      }
      case 403 => {
        println("Forbidden request")
        return (requestCall.code, requestCall.headers, requestCall.body)
      }
      case 401 => {
        sleep
        request(url)
      }
      case 408 => request(url)
      case 500 => request(url)
    }
  }

  def getRepositoryJson(repoName: String): (String, String) = {
    val url = api + "repos/" + repoName + "?access_token=" + authentication
    val (statusCode, headers, body) = request(url)
    return (prettyPrintJson(body), headers.get("ETag").get)
  }

  def getUserJson(login: String): (String, String) = {
    val url = api + "users/" + login + "?access_token=" + authentication
    val (statusCode, headers, body) = request(url)
    return (prettyPrintJson(body), headers.get("ETag").get)
  }

  def getIssues(repoName: String): (String, String) = {
    val (issues, cache) = getUsingPages(repoName, "issues", "", "")
    return (issues, cache)
  }

  def getComments(repoName: String): (String, String) = {
    val (comments, cache) = getUsingPages(repoName, "comments", "", "")
    return (comments, cache)
  }

  def getPullRequests(repoName: String): (String, String) = {
    val (pulls, cache) = getUsingPages(repoName, "pulls", "", "")
    return (pulls, cache)
  }

  def getPullRequest(repoName: String, pullRequest: Long): (String, String) = {
    val url = api_repo + repoName + "/pulls/" + pullRequest + "?access_token=" + authentication
    val (statusCode, headersMap, body) = request(url)

    return (prettyPrintJson(body), headersMap.get("ETag").get)
  }

  def getPatch(repoName: String, patch_url: String): (String) = {
    val (statusCode, headersMap, resultString) = request(patch_url)
    return resultString
  }

  def getBranchesCommits(repoName: String, dir: String, branchesList: Map[String, String]) {
    val branchesDirectory = new File(dir + "/branch/");
    if (!(branchesDirectory.exists && branchesDirectory.isDirectory))
      branchesDirectory.mkdirs() // create the branches directory
    //println(dir)
    val grouped = branchesList.keys.toList.grouped(threads)
    for (group <- grouped) {
      val groupList = group.toList
      val pool: ExecutorService = Executors.newFixedThreadPool(groupList.size)
      for (branch <- groupList) {
        //println("Retrieving commits for branch " + branch + " of " + repoName)
        var branchFile = branch
        if (branch.mkString.contains("/")) {
          branchFile = branch.replace("/", "-")
        }

        pool.execute(new Runnable {
          def run() {
            val (commits, cache) = getUsingPages(repoName, "commits", branchesList.get(branch).get, branch)
            printToFile(new File(dir + "/branch/" + branchFile + "-commits.json"))(f => f.println(commits))
            printToFile(new File(dir + "/cache/" + branchFile + "-commits.cache"))(f => f.println(cache))
          }
        })
      }
      pool.shutdown()
      pool.awaitTermination(2L, TimeUnit.HOURS)
    }
  }

  /**
   * Retrieve a json using pagination, and return the json and its etag
   */
  def getUsingPages(repoName: String, cmd: String, branchSHA: String, branchName: String): (String, String) = {
    var pageNumber = 1;

    if (branchSHA.nonEmpty) {
      println("Retrieving " + cmd + " from " + branchName + " of " + repoName)
    } else
      println("Retrieving " + cmd + " for " + repoName)
    var url = api_repo + repoName + "/" + cmd
    val stringBuilder = new StringBuilder
    //val parameterList = collection.mutable.Map[String, String]()
    //parameterList += ("per_page" -> "100")
    cmd match {
      case "issues"   => url = api_repo + repoName + "/issues?access_token=" + authentication + "&per_page=100&state=all"
      case "commits"  => url = api_repo + repoName + "/commits?access_token=" + authentication + "&per_page=100&sha=" + branchSHA
      case "pulls"    => url = api_repo + repoName + "/pulls?access_token=" + authentication + "&per_page=100&state=all"
      case "forks"    => url = api_repo + repoName + "/forks?access_token=" + authentication + "&per_page=100"
      case "comments" => url = api_repo + repoName + "/issues/comments?access_token=" + authentication + "&per_page=100"
      case "branches" => url = api_repo + repoName + "/branches?access_token=" + authentication + "&per_page=100"
    }

    //val paramList = parameterList.toMap
    val (statusCode, headersMap, resultString) = request(url)
    val etag = headersMap.get("ETag").get
    val link = headersMap.get("Link")
    if (!link.equals(None)) {
      //println("Using pagination")
      val splitLink = link.get.split(",")
      val nextPage = (splitLink(0).split(";")(0)).replaceAll("[<>]", "")
      val rel = splitLink(0).split(";")(1)
      var next = rel.contentEquals(""" rel="next"""")
      stringBuilder.append(prettyPrintJson(resultString))
      url = nextPage
      while (next) {
        val (statusCode1, headersMap1, result) = request(url)
        val linkNext = headersMap1.get("Link")
        //println(headersMap1.get("Link"))
        val splitLink1 = linkNext.get.split(",")
        val nextPage1 = (splitLink1(0).split(";")(0)).replaceAll("[<>]", "")
        val relNext = splitLink1(0).split(";")(1)
        //println(relNext)
        next = relNext.contentEquals(""" rel="next"""")
        //println(next)
        // it may be if there are fix number of commits multiple of 100, e.g. 200,300,400. Then github gives a final page with empty json []
        if (!result.contentEquals("[]")) {
          val resultString1 = prettyPrintJson(result)
          val lastBracketIndex = stringBuilder.lastIndexOf("]")
          stringBuilder.deleteCharAt(lastBracketIndex)
          stringBuilder.append(",")
          val j = resultString1.drop(2)
          stringBuilder.append(j)
          requests += 1
          url = nextPage1
        }
        if (requests > 100) {
          requests = 0
          sleep

        }
      }
    } else {
      stringBuilder.append(prettyPrintJson(resultString))
    }

    val jsonString = stringBuilder.toString
    return (jsonString, etag)
  }

  def prettyPrintJson(json: String): String = {
    try{
    gson.toJson(jsonParser.parse(json))}
    catch {
      // some retrieval of patches give a normal string, which cannot be parsed...
      case e : Exception => return json
    }
    
  }
  def getCache(cmd: String, dir: String): String = {
    return Source.fromFile(new File(dir + "/cache/" + cmd + ".cache")).getLines.mkString
  }

  def getBranchCache(cmd: String, dir: String, branch: String): String = {
    var branchFile = branch
    if (branch.contains("/")) {
      branchFile = branch.replaceAll("/", "-")
    }
    return Source.fromFile(new File(dir + "/cache/" + branchFile + "-commits.cache")).getLines.mkString
  }

  def wasModified(repoName: String, cmd: String, cache: String, branch: String): (Boolean) = {
    var url = api_repo + repoName
    cmd match {
      case "branches" => url = api_repo + repoName + "/branches?access_token=" + authentication + "&per_page=100"
      case "commits"  => url = api_repo + repoName + "/commits?access_token=" + authentication + "&per_page=100&sha=" + branch
      case "user"     => url = "https://api.github.com/users/" + repoName.split("/")(0) + "?access_token=" + authentication
      case "repo"     => url = api_repo + repoName + "?access_token=" + authentication
      case "issues"   => url = api_repo + repoName + "/issues?access_token=" + authentication + "&per_page=100&state=all"
      case "pulls" =>
        url = api_repo + repoName + "/pulls?access_token=" + authentication + "&per_page=100&state=all"; println()
      case "comments" => url = api_repo + repoName + "/issues/comments?access_token=" + authentication + "&per_page=100"
      case "forks"    => url = api_repo + repoName + "/forks?access_token=" + authentication + "&per_page=100"
      case _          => url = api_repo + repoName + "/" + cmd
    }
    sleep

    val request = Http(url).header(ETagHeader, cache.trim).options(HttpOptions.connTimeout(TIMEOUT), HttpOptions.readTimeout(TIMEOUT)).asString
    //val request = Http(url).header(ETagHeader, cache.trim).options(HttpOptions.connTimeout(TIMEOUT),
    //  HttpOptions.readTimeout(TIMEOUT)).asCodeHeaders
    //val etag = request._2.get("ETag").get(0)
    val modified = request.headers.get("Status").get.split(" ")(1)
    if (Integer.parseInt(modified) == 304)
      return (false)
    else return (true)

  }

  def getForksPerLevel(forks: List[String], dir: String) {
    for (fork <- forks) {
      getOrUpdateRepositoryData(fork, dir)
    }
  }

  def getOrUpdateRepositoryData(repoName: String, dir: String) {
    sleep
    println("Retrieving data for repository " + repoName)
    val repoFolder = repoName.replace("/", "-")
    val directory = new File(dir + "/" + repoFolder);
    val cacheDirectory = new File(directory.getPath + "/cache");
    val pullDirectory = new File(directory.getPath + "/pulls");
    val pullPatchesDirectory = new File(directory.getPath + "/pulls/patches");
    val pullDirectoryCache = new File(directory.getPath + "/pulls/cache");

    if (!directory.exists)
      directory.mkdirs
    if (!cacheDirectory.exists)
      cacheDirectory.mkdirs
    if (!pullDirectory.exists)
      pullDirectory.mkdirs
    if (!pullPatchesDirectory.exists)
      pullPatchesDirectory.mkdirs
    if (!pullDirectoryCache.exists)
      pullDirectoryCache.mkdirs

    // try to get the cache. if there is no cache file, then there is no file

    // *** REPOSITORY *** 
    try {
      val repoOldCache = getCache("repo", directory.getPath)
      val (modifiedRepo) = wasModified(repoName, "repo", repoOldCache, "")
      if (modifiedRepo) {
        println("Repository json was modified since last retrieval")
        val (repoJson, repoNewCache) = getRepositoryJson(repoName)
        printToFile(new File(directory.getPath + "/repo.json"))(f => f.println(repoJson))
        printToFile(new File(directory.getPath + "/cache/repo.cache"))(f => f.println(repoNewCache))
      } else {
        println("Repository json is the same")
      }
    } catch {
      case fileNotFound: FileNotFoundException => {
        val (repo, repoCache) = getRepositoryJson(repoName)
        printToFile(new File(directory.getPath + "/repo.json"))(f => f.println(repo))
        printToFile(new File(directory.getPath + "/cache/repo.cache"))(f => f.println(repoCache))
      }
    }

    // *** USER ***
    try {
      val userOldCache = getCache("user", directory.getPath)
      val (modifiedUser) = wasModified(repoName.split("/")(0), "user", userOldCache, "")
      if (modifiedUser) {
        println("User json was modified since last retrieval")
        val (userJson, userNewCache) = getUserJson(repoName.split("/")(0))
        printToFile(new File(directory.getPath + "/user.json"))(f => f.println(userJson))
        printToFile(new File(directory.getPath + "/cache/user.cache"))(f => f.println(userNewCache))
      } else {
        println("Repository user is the same")
      }

    } catch {
      case fileNotFound: FileNotFoundException => {
        val (user, userCache) = getUserJson(repoName.split("/")(0))
        printToFile(new File(directory.getPath + "/user.json"))(f => f.println(user))
        printToFile(new File(directory.getPath + "/cache/user.cache"))(f => f.println(userCache))
      }
    }
    // *** FORKS ***
    try {
      val forksOldCache = getCache("forks", directory.getPath)
      val (modifiedForks) = wasModified(repoName, "forks", forksOldCache, "")
      if (modifiedForks) {
        println("Forks json was modified since last retrieval")
        val (forksJson, forksNewCache) = getUsingPages(repoName, "forks", "", "")
        printToFile(new File(directory.getPath + "/forks.json"))(f => f.println(forksJson))
        printToFile(new File(directory.getPath + "/cache/forks.cache"))(f => f.println(forksNewCache))
      } else {
        println("Repository forks is the same")
      }

    } catch {
      case fileNotFound: FileNotFoundException => {
        val (forks, forksCache) = getUsingPages(repoName, "forks", "", "")
        printToFile(new File(directory.getPath + "/forks.json"))(f => f.println(forks))
        printToFile(new File(directory.getPath + "/cache/forks.cache"))(f => f.println(forksCache))
      }
    }
    // *** ISSUES ***
    try {
      val issuesCache = getCache("issues", directory.getPath)
      val (modifiedIssues) = wasModified(repoName, "issues", issuesCache, "")
      if (modifiedIssues) {
        println("Issues json was modified since last retrieval")
        val (issues, issuesNewCache) = getUsingPages(repoName, "issues", "", "")
        printToFile(new File(directory.getPath + "/issues.json"))(f => f.println(issues))
        printToFile(new File(directory.getPath + "/cache/issues.cache"))(f => f.println(issuesNewCache))
      } else {
        println("Repository issues is the same")
      }

    } catch {
      case fileNotFound: FileNotFoundException => {
        val (issues, issuesCache) = getIssues(repoName)
        printToFile(new File(directory.getPath + "/issues.json"))(f => f.println(issues))
        printToFile(new File(directory.getPath + "/cache/issues.cache"))(f => f.println(issuesCache))

      }
    }
    // *** COMMENTS ***
    try {
      val commentsOldCache = getCache("comments", directory.getPath)
      val (modifiedComments) = wasModified(repoName, "comments", commentsOldCache, "")

      if (modifiedComments) {
        println("Comments json was modified since last retrieval")
        val (commentsJson, commentsNewCache) = getUsingPages(repoName, "comments", "", "")
        printToFile(new File(directory.getPath + "/comments.json"))(f => f.println(commentsJson))
        printToFile(new File(directory.getPath + "/cache/comments.cache"))(f => f.println(commentsNewCache))

      } else {
        println("Repository comments is the same")
      }

    } catch {
      case fileNotFound: FileNotFoundException => {

        val (comments, commentsCache) = getComments(repoName)
        printToFile(new File(directory.getPath + "/comments.json"))(f => f.println(comments))
        printToFile(new File(directory.getPath + "/cache/comments.cache"))(f => f.println(commentsCache))

      }
    }
    // *** BRANCHES *** 
    try {
      val branchesOldCache = getCache("branches", directory.getPath)
      val (modifiedBranches) = wasModified(repoName, "branches", branchesOldCache, "")
      if (modifiedBranches) {
        println("Branches json was modified since last retrieval")
        val (branchesJson, branchesNewCache) = getUsingPages(repoName, "branches", "", "")
        printToFile(new File(directory.getPath + "/branches.json"))(f => f.println(branchesJson))
        printToFile(new File(directory.getPath + "/cache/branches.cache"))(f => f.println(branchesNewCache))
      } else {
        println("Repository branches is the same")
      }

    } catch {
      case fileNotFound: FileNotFoundException => {
        val (branchesJson, branchesCache) = getUsingPages(repoName, "branches", "", "")
        printToFile(new File(directory.getPath + "/branches.json"))(f => f.println(branchesJson))
        printToFile(new File(directory.getPath + "/cache/branches.cache"))(f => f.println(branchesCache))
      }
    }

    // *** FOR EACH BRANCH, CHECK COMMITS ***
    val modifiedBranchCommits = collection.mutable.Map[String, String]()
    try {
      val branchesList = readFromFile(directory.getPath + "/branches.json")
      val branchesListDecoded = branchesList.decodeOption[List[Branch]].get

      branchesListDecoded.foreach(branch => {
        try {
          val branchCommitsOldCache = readFromFile(directory.getPath + "/cache/" + branch.name + "-commits.cache")
          val (branchCommitsModified) = wasModified(repoName, "commits", branchCommitsOldCache, branch.name)
          if (branchCommitsModified) {
            modifiedBranchCommits += (branch.name -> branch.commit.sha)
          } else {
            println("Commits on branch " + branch.name + " of " + repoName + " is the same")
          }
        } catch {
          case fileNotFound: FileNotFoundException => {
            modifiedBranchCommits += (branch.name -> branch.commit.sha)
          }
        }
      })

    } catch {
      case fileNotFound: FileNotFoundException => {
        println("it seems that the branches json cannot be read from file")
      }
      case ex: Exception => {
        println("Cannot decode the branches json")
      }
    } finally {
      getBranchesCommits(repoName, directory.getPath, modifiedBranchCommits.toMap)
    }

    // *** PULL REQUESTS *** 
    try {
      val pullRequestsOldCache = getCache("pulls", directory.getPath)
      val (modifiedPullRequests) = wasModified(repoName, "pulls", pullRequestsOldCache, "")

      if (modifiedPullRequests) {
        /**
         * Get old pull requests, and retrieve new ones
         */
        println("Pull requests json was modified since last retrieval")
        val oldPullRequests = readFromFile(directory.getPath + "/pulls.json")

        val (newPullRequests, pullRequestsNewCache) = getUsingPages(repoName, "pulls", "", "")
        printToFile(new File(directory.getPath + "/pulls.json"))(f => f.println(newPullRequests))
        printToFile(new File(directory.getPath + "/cache/pulls.cache"))(f => f.println(pullRequestsNewCache))

        val oldPullRequestsDecoded = oldPullRequests.decodeOption[List[PullRequests]].get
        val newPullRequestsDecoded = newPullRequests.decodeOption[List[PullRequests]].get

        /**
         * For each old pull-request see if it was modified (it may have been closed since the last check) and get the new json if it was
         */
        oldPullRequestsDecoded.foreach { x =>
          try {
            val pullRequestOldCache = readFromFile(directory.getPath + "/pulls/cache/" + x.number + ".cache")
            val (pullRequestModified) = wasModified(repoName, "pulls/" + x.number, pullRequestOldCache, "")
            if (pullRequestModified) {
              println("Pull request number " + x.number + " was modified")
              val (newPullRequest, newPullRequestCache) = getPullRequest(repoName, x.number)
              printToFile(new File(directory.getPath + "/pulls/" + x.number + ".json"))(f => f.println(newPullRequest))
              printToFile(new File(directory.getPath + "/pulls/cache/" + x.number + ".cache"))(f => f.println(newPullRequestCache))
            }
          } catch {
            case fileNotFound: FileNotFoundException => {
              val (newPullRequest, newPullRequestCache) = getPullRequest(repoName, x.number)
              printToFile(new File(directory.getPath + "/pulls/" + x.number + ".json"))(f => f.println(newPullRequest))
              printToFile(new File(directory.getPath + "/pulls/cache/" + x.number + ".cache"))(f => f.println(newPullRequestCache))
            }
          }
        }

        /**
         *  Compare old pull-requests with new-ones and retrieve the new pull requests if any
         */
        val newRequests = newPullRequestsDecoded.diff(oldPullRequestsDecoded)
        newRequests.foreach { x =>
          val (request, etag) = getPullRequest(repoName, x.number)
          printToFile(new File(directory.getPath + "/pulls/" + x.number + ".json"))(f => f.println(request))
          printToFile(new File(directory.getPath + "/pulls/cache/" + x.number + ".cache"))(f => f.println(etag))
        }

      } else {
        println("Pull requests json is the same")
      }

    } catch {
      case fileNotFound: FileNotFoundException => {
        println("Retrieving pull requests")
        val (pullRequests, pullRequestsCache) = getUsingPages(repoName, "pulls", "", "")
        printToFile(new File(directory.getPath + "/pulls.json"))(f => f.println(pullRequests))
        printToFile(new File(directory.getPath + "/cache/pulls.cache"))(f => f.println(pullRequestsCache))
        val pulls = pullRequests.decodeOption[List[PullRequests]].get
        pulls.foreach(pullRequest => {
          val (pull, cache) = getPullRequest(repoName, pullRequest.number)
          val pullDecoded = pull.decodeOption[SinglePullRequest].get
          //val patch = getPatch(repoName, pullDecoded.patch_url)
          //printToFile(new File(pullPatchesDirectory + "/" + pullRequest.number + ".patch"))(f => f.println(patch))
          printToFile(new File(pullDirectory + "/" + pullRequest.number + ".json"))(f => f.println(prettyPrintJson(pull)))
          printToFile(new File(pullDirectoryCache + "/" + pullRequest.number + ".cache"))(f => f.println(prettyPrintJson(cache)))
        })

      }
    }
  }

  def callAll(repoName: String) {
    println("Retrieving " + repoName + "'s information. It may take a while")
    getOrUpdateRepositoryData(repoName, "github/")

    val repoFolder = repoName.replace("/", "-")
    val forksFolder = new File("github/" + repoFolder + "/forks").mkdirs
    val mainRepoForksJson = Source.fromFile(new File("github/" + repoName.replace("/", "-") + "/forks.json")).getLines.mkString
    val mainRepoForksList = mainRepoForksJson.decodeOption[List[MainRepository]].getOrElse(Nil).toList
    //println("forks " + mainRepoForksList.size)
    println(repoName + " has " + mainRepoForksList.size + " first level forks. Retrieving the rest of nested forks")
    //sleepForOneHourIfNeeded
    // Now we get the forks, and put them into levels 
    val level = "level-"
    var levelNumber = 1
    var forksStillExists = true
    val forksList = scala.collection.mutable.MutableList[String]()
    mainRepoForksList.foreach(f => forksList += f.full_name)

    println("Retrieving nested forks.")
    while (forksList.size != 0) {
      totalNumberOfForks += forksList.size
      val forksDirectory = new File("github/" + repoFolder + "/forks/" + level + levelNumber + "-forks");
      if (!forksDirectory.exists && !forksDirectory.isDirectory)
        forksDirectory.mkdirs()
      val forks = forksList.clone
      forksList.clear
      getForksPerLevel(forks.toList, forksDirectory.getPath)
      for (fork <- forksDirectory.listFiles) {
        val forksJson = (Source.fromFile(new File(fork.getPath + "/forks.json")).getLines.mkString)
        val forks = forksJson.decodeOption[List[MainRepository]].getOrElse(Nil).toList
        forks.foreach(f => forksList += f.full_name)
      }
      levelNumber += 1
    }
    println("Total number of forks for " + repoName + " is " + (totalNumberOfForks))
  }

  def tokens() {

    if (token2.equals("token"))
      token2 = token1
    if (token3.equals("token"))
      token3 = token1
    if (token4.equals("token"))
      token4 = token1
  }

  def getLimitRate(token: String): String = {

    val url = "https://api.github.com/rate_limit?access_token=" + token
    val (statusCode, headers, response) = request(url)
    return prettyPrintJson(response)
  }

  // sleep if reached limit for both credentials. otherwise, do not sleep and switch credentials

  def sleep {

    currentToken = authentication
    if (getLimitRate(currentToken).decodeOption[Resources].get.resources.core.remaining < 300) {

      val auth1LimitJson = getLimitRate(token1).decodeOption[Resources].get
      val auth2LimitJson = getLimitRate(token2).decodeOption[Resources].get
      val auth3LimitJson = getLimitRate(token3).decodeOption[Resources].get
      val auth4LimitJson = getLimitRate(token4).decodeOption[Resources].get

      val auth1LimitRemaining = auth1LimitJson.resources.core.remaining
      val auth2LimitRemaining = auth2LimitJson.resources.core.remaining
      val auth3LimitRemaining = auth3LimitJson.resources.core.remaining
      val auth4LimitRemaining = auth4LimitJson.resources.core.remaining

      val auth1Reset = auth1LimitJson.resources.core.reset
      val auth2Reset = auth2LimitJson.resources.core.reset
      val auth3Reset = auth3LimitJson.resources.core.reset
      val auth4Reset = auth4LimitJson.resources.core.reset

      //val mapReset = Map[String, Long](token1 -> auth1Reset, token2 -> auth2Reset, token3 -> auth3Reset, token4 -> auth4Reset)
      val mapReset = Map[Long, String](auth1Reset -> token1, auth2Reset -> token2, auth3Reset -> token3, auth4Reset -> token4)
      //val mapLimit = Map[String, Int](token1 -> auth1LimitRemaining, token2 -> auth2LimitRemaining, token3 -> auth3LimitRemaining, token4 -> auth4LimitRemaining)
      val mapLimit = Map[Int, String](auth1LimitRemaining -> token1, auth2LimitRemaining -> token2, auth3LimitRemaining -> token3, auth4LimitRemaining -> token4)

      //val maxRemainingQueries = max(mapLimit.keys.toList).get
      val maxRemainingQueries = mapLimit.keys.toList.max
      println(maxRemainingQueries + "--" + mapLimit.get(maxRemainingQueries).get)
      if (maxRemainingQueries > 300) {
        authentication = mapLimit.get(maxRemainingQueries).get
        println("Switching authentication to " + authentication)

      } else {
        // find the minimal time to wait for reset of queries
        val minRemainingWaitTime = mapReset.keys.toList.min
        authentication = mapReset.get(minRemainingWaitTime).get
        println("Switching authentication to " + authentication)

        // sleep
        val sleepTime = getRemainingSeconds(minRemainingWaitTime) + 100
        println("Reached limit. Sleeping for " + sleepTime + " seconds")
        Thread.sleep((sleepTime) * 1000)
      }

    }
  }

  def getRemainingSeconds(value: Long): Long = {
    val currentTime = System.currentTimeMillis / 1000
    return (value - currentTime)
  }

  def minValue(l: Long, r: Long): (Long) = {
    if (r >= l) l else r
  }

  def run(in: String): (List[String], List[String], Int) = {
    var out = List[String]()
    var err = List[String]()
    val process = Process(in)
    val exit = process ! ProcessLogger((s) => out ::= s, (s) => err ::= s)
    (out.reverse, err.reverse, exit)
  }
  // copyright http://stackoverflow.com/questions/4604237/how-to-write-to-a-file-in-scala
  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) = {
    val p = new java.io.PrintWriter(f)
    try {
      op(p)
    } finally {
      p.close
    }
  }

  def main(args: Array[String]) {
    var repoName = ""
    sleep
    val startedTime = System.currentTimeMillis()

    if (args.size == 0 || args.size == 1) {
      println(" You need to give two arguments to the call of the jar")
      println(" [repository] [option] --- e.g. java -jar GithubExtractor.jar scas-mdd/test --retrieve")
      println(" Option   --retrieve; Retrieves repository meta-data from Github ")
      println("          --populateDB; Creates a table with information from json files of the repository ")
      println("          --all; Combines retrieval, and populating database")
      println("          --main; same as all but only for the main repository, no forks are downloaded")
    }

    if (args.size > 1) {
      repoName = args(0)
      if (args(1).equals("--retrieve")) {
        callAll(repoName)
      }

      if (args(1).equals("--populateDB")) {
        populateDatabase(repoName)
      }
      if (args.size == 2 && args(1).equals("--all")) {
        callAll(repoName)
        populateDatabase(repoName)
      }
      if (args(1).equals("--main")) {
        getOrUpdateRepositoryData(repoName, "github/")
        populateDatabase(repoName)
      }
    }

    //sleep

    val finishedTime = System.currentTimeMillis
    println("Finished in " + ((finishedTime - startedTime) / 1000) + " seconds.")

  }
}

case class Repository(id: Int, full_name: String, owner: Owner, forks_url: String, created_at: String, updated_at: String, pushed_at: String, forks_count: Int, parent: Parent)
object Repository {
  implicit def RepositoryCodecJson: CodecJson[Repository] = casecodec9(Repository.apply, Repository.unapply)("id", "full_name", "owner", "forks_url", "created_at", "updated_at", "pushed_at", "forks_count", "parent")
}

case class MainRepository(id: Int, full_name: String, owner: Owner, forks_url: String, created_at: String, updated_at: String, pushed_at: String, forks_count: Int)
object MainRepository {
  implicit def MainRepositoryCodecJson: CodecJson[MainRepository] = casecodec8(MainRepository.apply, MainRepository.unapply)("id", "full_name", "owner", "forks_url", "created_at", "updated_at", "pushed_at", "forks_count")
}
case class Parent(full_name: String)
object Parent {
  implicit def ParentCodecJson: CodecJson[Parent] = casecodec1(Parent.apply, Parent.unapply)("full_name")
}
case class Owner(login: String)
object Owner {
  implicit def OwnerCodecJson: CodecJson[Owner] = casecodec1(Owner.apply, Owner.unapply)("login")
}

case class Commits(sha: String, commit: Option[Commit])

object Commits {
  implicit def CommitsCodecJson: CodecJson[Commits] = casecodec2(Commits.apply, Commits.unapply)("sha", "commit")
}

case class Commit(author: Option[Author], committer: Option[Committer], message: String)

object Commit {
  implicit def CommitCodecJson: CodecJson[Commit] = casecodec3(Commit.apply, Commit.unapply)("author", "committer", "message")
}

case class Author(name: String, email: String, date: String)
object Author {
  implicit def AuthorCodecJson: CodecJson[Author] = casecodec3(Author.apply, Author.unapply)("name", "email", "date")
}

case class Committer(name: String, email: String, date: String)
object Committer {
  implicit def CommitterCodecJson: CodecJson[Committer] = casecodec3(Committer.apply, Committer.unapply)("name", "email", "date")
}

case class Resources(resources: Resource)
object Resources {
  implicit def ResourcesCodecJson: CodecJson[Resources] = casecodec1(Resources.apply, Resources.unapply)("resources")
}
case class Resource(core: Core)
object Resource {
  implicit def ResourceCodecJson: CodecJson[Resource] = casecodec1(Resource.apply, Resource.unapply)("core")
}
case class Core(remaining: Int, reset: Long)
object Core {
  implicit def CoreCodecJson: CodecJson[Core] = casecodec2(Core.apply, Core.unapply)("remaining", "reset")
}

case class User(login: String, id: Int, name: String, email: String, created_at: String)
object User {
  implicit def UserCodecJson: CodecJson[User] = casecodec5(User.apply, User.unapply)("login", "id", "name", "email", "created_at")
}
case class UserNoEmail(login: String, id: Int, name: String, created_at: String)
object UserNoEmail {
  implicit def UserNoEmailCodecJson: CodecJson[UserNoEmail] = casecodec4(UserNoEmail.apply, UserNoEmail.unapply)("login", "id", "name", "created_at")
}
case class UserNoName(login: String, id: Int, created_at: String)
object UserNoName {
  implicit def UserNoNameCodecJson: CodecJson[UserNoName] = casecodec3(UserNoName.apply, UserNoName.unapply)("login", "id", "created_at")
}

case class SimpleUser(login: String, id: Int)

object SimpleUser {
  implicit def SimpleUserCodecJson: CodecJson[SimpleUser] = casecodec2(SimpleUser.apply, SimpleUser.unapply)("login", "id")
}

case class Branch(name: String, commit: BranchCommit)
object Branch {
  implicit def BranchCodecJson: CodecJson[Branch] = casecodec2(Branch.apply, Branch.unapply)("name", "commit")
}

case class BranchCommit(sha: String, url: String)
object BranchCommit {
  implicit def BranchCommitCodecJson: CodecJson[BranchCommit] = casecodec2(BranchCommit.apply, BranchCommit.unapply)("sha", "url")
}

/*
case class Issues(url: String )
object Issues{
  implicit def IssuesCodecJson: CodecJson[Issues] = casecodec1(Issues.apply, Issues.unapply)("url")
}*/
case class Issues(id: Int, number: Int, title: String, user: SimpleUser, state: String, comments: Int, created_at: String, updated_at: Option[String], closed_at: Option[String], pull_request: Option[IssuePullRequest], body: Option[String])
object Issues {
  implicit def IssuesCodecJson: CodecJson[Issues] = casecodec11(Issues.apply, Issues.unapply)("id", "number", "title", "user", "state", "comments", "created_at", "updated_at", "closed_at", "pull_request", "body")
}

case class IssuePullRequest(url: String)
object IssuePullRequest {
  implicit def IssuePullRequestCodecJson: CodecJson[IssuePullRequest] = casecodec1(IssuePullRequest.apply, IssuePullRequest.unapply)("url")
}

case class SinglePullRequest(id: Long, patch_url: String, number: Long, state: String, title: String, user: Option[SimpleUser], body: Option[String], created_at: String, updated_at: Option[String], closed_at: Option[String], head: Option[Head], merged_at: Option[String], merge_commit_sha: Option[String], merged: Boolean, mergeable: Option[Boolean], merged_by: Option[SimpleUser], comments: Option[Long], commits: Option[Long], additions: Option[Long], deletions: Option[Long], changed_files: Option[Long])
object SinglePullRequest {
  implicit def PullRequestsCodecJson: CodecJson[SinglePullRequest] = casecodec21(SinglePullRequest.apply, SinglePullRequest.unapply)("id", "patch_url", "number", "state", "title", "user", "body", "created_at", "updated_at", "closed_at", "head", "merged_at", "merge_commit_sha", "merged", "mergeable", "merged_by", "comments", "commits", "additions", "deletions", "changed_files")
}

case class PullRequests(id: Long, number: Long, state: String, title: String, user: Option[SimpleUser], body: String, created_at: String, updated_at: Option[String], closed_at: Option[String], merged_at: Option[String], merge_commit_sha: Option[String], head: Option[Head], base: Option[Base])
object PullRequests {
  implicit def PullRequestsCodecJson: CodecJson[PullRequests] = casecodec13(PullRequests.apply, PullRequests.unapply)("id", "number", "state", "title", "user", "body", "created_at", "updated_at", "closed_at", "merged_at", "merge_commit_sha", "head", "base")
}
case class SimpleRepository(id: Long, full_name: String)
object SimpleRepository {
  implicit def SimpleRepositoryCodecJson: CodecJson[SimpleRepository] = casecodec2(SimpleRepository.apply, SimpleRepository.unapply)("id", "full_name")
}

case class Head(user: SimpleUser, repo: Option[SimpleRepository])
object Head {
  implicit def HeadCodecJson: CodecJson[Head] = casecodec2(Head.apply, Head.unapply)("user", "repo")
}
case class Base(user: SimpleUser)
object Base {
  implicit def BaseCodecJson: CodecJson[Base] = casecodec1(Base.apply, Base.unapply)("user")
}

// There are two types of comments - commit comments and issue comments. The commit comments have a commit-id. 
case class Comments(html_url: String, id: Long, user: SimpleUser, commit_id: Option[String], created_at: String, updated_at: Option[String], body: Option[String])
object Comments {
  implicit def CommentsCodecJson: CodecJson[Comments] = casecodec7(Comments.apply, Comments.unapply)("html_url", "id", "user", "commit_id", "created_at", "updated_at", "body")
}

