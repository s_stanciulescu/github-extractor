package json

import org.junit.Test

import org.junit.Assert._
import argonaut._, Argonaut._
import dk.itu.scas.github.extractor.GithubExtractor._
import dk.itu.scas.github.extractor._
import dk.itu.scas.github.extractor.util._


class PullRequestsDecoding {


  val pullRequests = Util.readFromFile("resources/pull-requests.json")
  val pullRequests1 = Util.readFromFile("resources/pulls.json")
 // val retrievedPullRequests = getUsingPages("kliment/Sprinter", "pulls", "")
  @Test
  def testParsing(){
    assertTrue(pullRequests.parse.validation.isSuccess)
    assertTrue(pullRequests1.parse.validation.isSuccess)
   // assertTrue(retrievedPullRequests._1.parse.validation.isSuccess)
  }
  @Test
  def testDecoding(){
    val result = pullRequests.decodeValidation[List[PullRequests]]
    val result1 = pullRequests1.decodeValidation[List[PullRequests]]
    //val retrievedPR = retrievedPullRequests._1.decodeValidation[List[PullRequests]]
    assertTrue(result.isSuccess)
    assertTrue(result1.isSuccess)
    
   // assertTrue(retrievedPR.isSuccess)
  }
  @Test
  def testPullRequests(){
    assertEquals(30, pullRequests.decodeOption[List[PullRequests]].get.size)
    assertEquals(487, pullRequests1.decodeOption[List[PullRequests]].get.size)
    //assertEquals(171, retrievedPullRequests._1.decodeOption[List[PullRequests]].get.size)
  }
}