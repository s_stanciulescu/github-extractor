package etags

import org.junit.Test
import org.junit.Assert._
import dk.itu.scas.github.extractor.GithubExtractor._
import dk.itu.scas.github.extractor._
import dk.itu.scas.github.extractor.util._
import dk.itu.scas.github.extractor.util.Util._
import argonaut._
import Argonaut._
import java.io.File

class EtagsTest {

  
  
  @Test
  def noChanges(){
    val cache = readFromFile("resources/etags/repo.cache")
    val commitsCache = readFromFile("resources/etags/master-commits.cache")
    
    //Thread.sleep(1000 * 130) // sleep 130 seconds
    assertFalse(wasModified("tonokip/Tonokip-Firmware", "repo", cache, ""))
    assertFalse(wasModified("tonokip/Tonokip-Firmware", "repo", cache, "master"))
    
  }
  
  
  @Test
  def pullRequestsCaching {
    val (prq, cache) = getUsingPages("kliment/Sprinter", "pulls", "","")
    printToFile(new File("resources/etags/pulls.cache"))(f => f.println(cache))
    val modified = wasModified("kliment/Sprinter", "pulls", cache, "")
    
    assertFalse(modified)
    assertEquals(modified, cache)
    
    Thread.sleep(1000 * 20)
    assertFalse(wasModified("kliment/Sprinter", "pulls", cache, ""))
           
  }
  
}